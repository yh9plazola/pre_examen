package Modelo;

public class Cliente 
{
    private String nombreCliente;
    private String fechaNacimiento;
    private String domicilio;
    private String sexo;
    
    public Cliente()
    {
        this.nombreCliente = "";
        this.fechaNacimiento = "";
        this.domicilio = "";
        this.sexo = "";
    }

    public Cliente(String nombreCliente, String fechaNacimiento, String domicilio, String sexo) 
    {
        this.nombreCliente = nombreCliente;
        this.fechaNacimiento = fechaNacimiento;
        this.domicilio = domicilio;
        this.sexo = sexo;
    }
    
    public Cliente(Cliente obj)
    {
        this.nombreCliente = obj.nombreCliente;
        this.fechaNacimiento = obj.fechaNacimiento;
        this.domicilio = obj.domicilio;
        this.sexo = obj.sexo;
    }

    public String getNombreCliente() 
    {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) 
    {
        this.nombreCliente = nombreCliente;
    }

    public String getFechaNacimiento() 
    {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getDomicilio() 
    {
        return domicilio;
    }

    public void setDomicilio(String domicilio) 
    {
        this.domicilio = domicilio;
    }

    public String getSexo() 
    {
        return sexo;
    }

    public void setSexo(String sexo) 
    {
        this.sexo = sexo;
    }
}
