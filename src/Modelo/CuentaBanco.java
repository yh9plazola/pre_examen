package Modelo;

public class CuentaBanco 
{
    private int numCuenta;
    private Cliente cliente;
    private String fechaApertura;
    private String nombreBanco;
    private int porcentajeRendimiento;
    private float saldo;
    
    public CuentaBanco()
    {
        this.numCuenta = 0;
        this.cliente = null;
        this.fechaApertura = "";
        this.nombreBanco = "";
        this.porcentajeRendimiento = 0;
        this.saldo = 0.0f;
    }

    public CuentaBanco(int numCuenta, Cliente cliente, String fechaApertura, String nombreBanco, int porcentajeRendimiento, float saldo) 
    {
        this.numCuenta = numCuenta;
        this.cliente = cliente;
        this.fechaApertura = fechaApertura;
        this.nombreBanco = nombreBanco;
        this.porcentajeRendimiento = porcentajeRendimiento;
        this.saldo = saldo;
    }
    
    public CuentaBanco(CuentaBanco obj)
    {
        this.numCuenta = obj.numCuenta;
        this.cliente = obj.cliente;
        this.fechaApertura = obj.fechaApertura;
        this.nombreBanco = obj.nombreBanco;
        this.porcentajeRendimiento = obj.porcentajeRendimiento;
        this.saldo = obj.saldo;
    }

    public int getNumCuenta() 
    {
        return numCuenta;
    }

    public void setNumCuenta(int numCuenta) 
    {
        this.numCuenta = numCuenta;
    }

    public Cliente getCliente() 
    {
        return cliente;
    }

    public void setCliente(Cliente cliente) 
    {
        this.cliente = cliente;
    }

    public String getFechaApertura() 
    {
        return fechaApertura;
    }

    public void setFechaApertura(String fechaApertura) 
    {
        this.fechaApertura = fechaApertura;
    }

    public String getNombreBanco() 
    {
        return nombreBanco;
    }

    public void setNombreBanco(String nombreBanco) 
    {
        this.nombreBanco = nombreBanco;
    }

    public int getPorcentajeRendimiento() 
    {
        return porcentajeRendimiento;
    }

    public void setPorcentajeRendimiento(int porcentajeRendimiento) 
    {
        this.porcentajeRendimiento = porcentajeRendimiento;
    }

    public float getSaldo() 
    {
        return saldo;
    }

    public void setSaldo(float saldo) 
    {
        this.saldo = saldo;
    }
    
    public void depositar(float cantidad)
    {
        this.saldo += cantidad;
    }
    
    public Boolean retirar(float cantidad)
    {
        if(this.saldo >= cantidad)
        {
            this.saldo -= cantidad;
            return true;
        }
        return false;
    }
    
    public float calcularReditos()
    {
        return this.porcentajeRendimiento * (this.saldo / 365);
    }
}
