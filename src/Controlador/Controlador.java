package Controlador;

import Modelo.Cliente;
import Modelo.CuentaBanco;
import Vista.dlgBanco;

import javax.swing.JOptionPane;
import javax.swing.JFrame;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ButtonModel;

public class Controlador implements ActionListener
{
    private CuentaBanco cuenta;
    private dlgBanco vista;
    
    public Controlador(dlgBanco vista, CuentaBanco cuenta)
    {
        this.vista = vista;
        this.cuenta = cuenta;
        // Escuchar eventos
        this.vista.btnNuevo.addActionListener(this);
        this.vista.btnGuardar.addActionListener(this);
        this.vista.btnMostrar.addActionListener(this);
        this.vista.btnDepositar.addActionListener(this);
        this.vista.btnRetirar.addActionListener(this);
    }
    
    public static void main(String[] args)
    {
        dlgBanco vista = new dlgBanco(new JFrame(), true);
        CuentaBanco cuenta = new CuentaBanco();
        Controlador controlador = new Controlador(vista, cuenta);
        controlador.inicializarVista();
    }
    
    private void inicializarVista()
    {
        this.vista.setTitle("Banco");
        this.vista.setSize(527, 412);
        this.vista.setLocationRelativeTo(null);
        this.vista.setVisible(true);
    }
    
    private void habilitarComponentes(Boolean bool)
    {
        // Botones
        this.vista.btnGuardar.setEnabled(bool);
        this.vista.btnMostrar.setEnabled(bool);
        this.vista.btnDepositar.setEnabled(!bool);
        this.vista.btnRetirar.setEnabled(!bool);
        // TextFields
        this.vista.txtNumCuenta.setEnabled(bool);
        this.vista.txtNombre.setEnabled(bool);
        this.vista.txtDomicilio.setEnabled(bool);
        this.vista.txtFechaNacimiento.setEnabled(bool);
        this.vista.txtNombreBanco.setEnabled(bool);
        this.vista.txtFechaApertura.setEnabled(bool);
        this.vista.txtPorcentaje.setEnabled(bool);
        this.vista.txtSaldo.setEnabled(bool);
        this.vista.txtCantidad.setEnabled(!bool);
        // RadioButtons
        this.vista.rdbMasculino.setEnabled(bool);
        this.vista.rdbFemenino.setEnabled(bool);
    }
    
    private void limpiar()
    {
        this.vista.txtNumCuenta.setText("");
        this.vista.txtNombre.setText("");
        this.vista.txtDomicilio.setText("");
        this.vista.txtFechaNacimiento.setText("");
        this.vista.txtNombreBanco.setText("");
        this.vista.txtFechaApertura.setText("");
        this.vista.txtPorcentaje.setText("");
        this.vista.txtSaldo.setText("");
        this.vista.txtCantidad.setText("");
    }
    
    private Boolean isVacio()
    {
        return this.vista.txtNumCuenta.getText().equals("") ||
               this.vista.txtNombre.getText().equals("") ||
               this.vista.txtDomicilio.getText().equals("") ||
               this.vista.txtFechaNacimiento.getText().equals("") ||
               this.vista.txtNombreBanco.getText().equals("") ||
               this.vista.txtFechaApertura.getText().equals("") ||
               this.vista.txtPorcentaje.getText().equals("") ||
               this.vista.txtSaldo.getText().equals("");
    }
    
    private Boolean isValido()
    {
        try
        {
            if(Integer.parseInt(this.vista.txtNumCuenta.getText()) < 0 ||
              (Integer.parseInt(this.vista.txtPorcentaje.getText()) < 0 ||
               Integer.parseInt(this.vista.txtPorcentaje.getText()) > 100) ||
               Float.parseFloat(this.vista.txtSaldo.getText()) < 0)
            {
                JOptionPane.showMessageDialog(this.vista, "No debe haber números negativos ni el porcentaje mayor al 100", ":: BANCO ::", JOptionPane.WARNING_MESSAGE);
                return false;
            }
        }
        catch(NumberFormatException ex)
        {
            JOptionPane.showMessageDialog(this.vista, "Ocurrio el siguiente error: " + ex.getMessage(), ":: BANCO ::", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) 
    {
        if(e.getSource() == this.vista.btnNuevo)
        {
            this.limpiar();
            this.habilitarComponentes(true);
        }
        if(e.getSource() == this.vista.btnGuardar)
        {
            if(this.isVacio())
            {
                JOptionPane.showMessageDialog(this.vista, "No puede haber campos vacios", ":: BANCO ::", JOptionPane.WARNING_MESSAGE);
                return;
            }
            if(!this.isValido())
            {
                return;
            }
            String sexo;
            if(this.vista.rdbMasculino.isSelected())
            {
                sexo = "Masculino";
            }
            else
            {
                sexo = "Femenino";
            }
            this.cuenta.setNumCuenta(Integer.parseInt(this.vista.txtNumCuenta.getText()));
            this.cuenta.setCliente(new Cliente(this.vista.txtNombre.getText(),
                                               this.vista.txtFechaNacimiento.getText(),
                                               this.vista.txtDomicilio.getText(), 
                                               sexo));
            this.cuenta.setFechaApertura(this.vista.txtFechaApertura.getText());
            this.cuenta.setNombreBanco(this.vista.txtNombreBanco.getText());
            this.cuenta.setPorcentajeRendimiento(Integer.parseInt(this.vista.txtPorcentaje.getText()));
            this.cuenta.setSaldo(Float.parseFloat(this.vista.txtSaldo.getText()));
            this.habilitarComponentes(false);
            this.vista.btnMostrar.setEnabled(true);
            this.vista.btnDepositar.setEnabled(true);
            this.vista.btnRetirar.setEnabled(true);
            this.vista.txtCantidad.setEnabled(true);
            JOptionPane.showMessageDialog(this.vista, "Se realizó la apertura de la cuenta con éxito", ":: Banco ::", JOptionPane.INFORMATION_MESSAGE);
        }
        if(e.getSource() == this.vista.btnMostrar)
        {
            this.vista.txtNumCuenta.setText(String.valueOf(this.cuenta.getNumCuenta()));
            this.vista.txtNombre.setText(this.cuenta.getCliente().getNombreCliente());
            this.vista.txtDomicilio.setText(this.cuenta.getCliente().getDomicilio());
            this.vista.txtFechaNacimiento.setText(this.cuenta.getCliente().getFechaNacimiento());
            if(this.cuenta.getCliente().getSexo() == "Masculino")
            {
                this.vista.rdbMasculino.setSelected(true);
            }
            else
            {
                this.vista.rdbFemenino.setSelected(true);
            }
            this.vista.txtNombreBanco.setText(this.cuenta.getNombreBanco());
            this.vista.txtFechaApertura.setText(this.cuenta.getFechaApertura());
            this.vista.txtPorcentaje.setText(String.valueOf(this.cuenta.getPorcentajeRendimiento()));
            this.vista.txtSaldo.setText(String.valueOf(this.cuenta.getSaldo()));
        }
        if(e.getSource() == this.vista.btnDepositar)
        {
            if(this.vista.txtCantidad.getText().equals(""))
            {
                JOptionPane.showMessageDialog(this.vista, "No puede haber campos vacios", ":: BANCO ::", JOptionPane.WARNING_MESSAGE);
                return;
            }
            try
            {
                if(Float.parseFloat(this.vista.txtCantidad.getText()) < 0)
                {
                    JOptionPane.showMessageDialog(this.vista, "No puede haber números negativos", ":: BANCO ::", JOptionPane.WARNING_MESSAGE);
                    return;
                }
            }
            catch(NumberFormatException ex)
            {
                JOptionPane.showMessageDialog(this.vista, "Ocurrio el siguiente error: " + ex.getMessage(), ":: BANCO ::", JOptionPane.ERROR_MESSAGE);
                return;
            }
            this.cuenta.depositar(Float.parseFloat(this.vista.txtCantidad.getText()));
            this.vista.txtNuevoSaldo.setText(String.valueOf(this.cuenta.getSaldo()));
            JOptionPane.showMessageDialog(this.vista, "Se ha depositado con exito", ":: BANCO ::", JOptionPane.WARNING_MESSAGE);
        }
        if(e.getSource() == this.vista.btnRetirar)
        {
            if(this.vista.txtCantidad.getText().equals(""))
            {
                JOptionPane.showMessageDialog(this.vista, "No puede haber campos vacios", ":: BANCO ::", JOptionPane.WARNING_MESSAGE);
                return;
            }
            try
            {
                if(Float.parseFloat(this.vista.txtCantidad.getText()) < 0)
                {
                    JOptionPane.showMessageDialog(this.vista, "No puede haber números negativos", ":: BANCO ::", JOptionPane.WARNING_MESSAGE);
                    return;
                }
            }
            catch(NumberFormatException ex)
            {
                JOptionPane.showMessageDialog(this.vista, "Ocurrio el siguiente error: " + ex.getMessage(), ":: BANCO ::", JOptionPane.ERROR_MESSAGE);
                return;
            }
            if(this.cuenta.retirar(Float.parseFloat(this.vista.txtCantidad.getText())))
            {
                this.vista.txtNuevoSaldo.setText(String.valueOf(this.cuenta.getSaldo()));
                JOptionPane.showMessageDialog(this.vista, "Se ha retirado con exito", ":: BANCO ::", JOptionPane.WARNING_MESSAGE);
            }
            else
            {
                JOptionPane.showMessageDialog(this.vista, "No hay dinero suficiente", ":: BANCO ::", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
